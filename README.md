# clojourney

This is a simple, tile map based game - or at least it may be one in the future.

So far it just renderes a simple map using hand-drawn tiles:

![first version with graphic tiles](first-version-with-graphic-tiles.png)

## Usage

Emacs - run cider, open `core.clj` and press `C-c C-k` to evaluate the file.

REPL - run `(require 'clojourney.core)`.

## License

Copyright © 2021 Julian Leyh <julian@vgai.de>

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
