(defproject clojourney "0.1.0-SNAPSHOT"
  :description "Simple tile map based game"
  :url "https://codeberg.org/rommudoh/clojourney"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [quil "3.1.0"]])
