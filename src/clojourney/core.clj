(ns clojourney.core
  (:require [quil.core :as q]
            [quil.middleware :as m]))

;; tile width/height
(def tile-width 32)
(def tile-height 32)

;; for beginning, start with a small, static map
(def simple-map-str
  ;;0...4....90...4....9
  ["1---------------2   "
   "|...............|   "
   "|...............|   "
   "|...............3--2"
   "|..................|"
   "3----2.............|"
   "     |.............|"
   "     |.............|"
   "     |.............|"
   "     |.............|"
   "  1--4.............|"
   "  |................|"
   "  |................|"
   "  3------2.........|"
   "         3---------4"])

;; bigger map than screen
(def bigger-map-str
  ;;0...4....90...4....90...4....90...4....9
  ["1---------------2   1---------------2   "
   "|...............|   |...............|   "
   "|...............|   |...............|   "
   "|...............3---4...............3--2"
   "|......................................|"
   "3----2.................................|"
   "     |.................................|"
   "     |.................................|"
   "     |.................................|"
   "     |.................................|"
   "  1--4.................................|"
   "  |....................................|"
   "  |....................................|"
   "  3-----2..............................|"
   "        |....................1---------4"
   "        |....................|          "
   "        3--------------------4          "])

;; convert strings to vectors of tiles
(def char->tile
  {\space {:id nil :walkable? false}
   \- {:id :vertical-wall :walkable? false}
   \| {:id :horizontal-wall :walkable? false}
   \. {:id :floor :walkable? true}
   \1 {:id :se-corner :walkable? false}
   \2 {:id :sw-corner :walkable? false}
   \3 {:id :ne-corner :walkable? false}
   \4 {:id :nw-corner :walkable? false}})

(defn str->tilemap [s]
  (mapv (partial mapv char->tile) s))

(defn load-tiles []
  (->> char->tile
     vals
     (map :id)
     (filter (complement nil?))
     (map (fn [tile] [tile (q/load-image (str "tiles/" (name tile) ".png"))]))
     (into {})))

(defn load-sprites []
  {:hero
   (into {} (for [dir [:north :south :west :east]]
              [dir (vec (for [frame (range 4)]
                          (q/load-image (str "sprites/hero-" (name dir) "-" frame ".png"))))]))})

(defn setup []
  (q/frame-rate 30)
  {:tilemap (str->tilemap bigger-map-str)
   :tiles (load-tiles)
   :x-offset 0
   :y-offset 0
   :sprite-images (load-sprites)
   :player {:id :hero
            :x (/ (q/width) 2)
            :y (/ (q/height) 2)
            :direction :south
            :current-frame 0
            :moving? false
            :speed 0.3
            :update-counter 0.0
            :delta 4}})

(defn get-tile-at [state x y]
  (let [xpos (int (/ x tile-width))
        ypos (int (/ y tile-width))]
    (get-in state [:tilemap ypos xpos])))

(defn move-in-direction [{:keys [x y direction delta] :as sprite} state]
  (let [new-x (case direction :east (+ x delta) :west (- x delta) x)
        new-y (case direction :south (+ y delta) :north (- y delta) y)]
    (if (:walkable? (get-tile-at state new-x new-y))
      (merge sprite {:x new-x :y new-y})
      sprite)))

(defn move-sprite [{:keys [id direction delta x y] :as sprite} state]
  (if (>= (:update-counter sprite) 1.0)
    (let [frame-count (count (get-in state [:sprite-images id direction]))]
      (-> sprite
         (update :current-frame #(-> % inc (mod frame-count)))
         (move-in-direction state)
         (update :update-counter - 1.0)
         (recur state)))
    sprite))

(defn update-sprite [state sprite]
  (if (:moving? sprite)
    (-> sprite
       (update :update-counter + (:speed sprite))
       (move-sprite state))
    (assoc sprite :update-counter 0)))

(defn start-moving [sprite direction]
  (-> sprite
     (assoc :direction direction)
     (assoc :moving? true)))

(defn handle-keys [state]
  ;; to fix bug when key-pressed? is stuck to true, check if key-code is zero, too
  ;; -> this most likely won't work in cljs, where the bug isn't present, but key-code is non-zero when keys are pressed
  (if (and (q/key-pressed?) (zero? (q/key-code)))
    (case (q/raw-key)
      \a (update state :player start-moving :west)
      \s (update state :player start-moving :south)
      \d (update state :player start-moving :east)
      \w (update state :player start-moving :north)
      state)
    (assoc-in state [:player :moving?] false)))

(defn update-state [state]
  (-> state
     (handle-keys)
     (update :player (partial update-sprite state))))

(defn draw-map [{:keys [tilemap tiles x-offset y-offset]}]
  ;; fill the empty rectangles black
  (q/fill 0)
  (q/image-mode :corner)
  (doseq [x (range (count (first tilemap)))
          y (range (count tilemap))]
    (let [tile (get-in tilemap [y x :id])
          xpos (- (* x tile-width) x-offset)
          ypos (- (* y tile-height) y-offset)]
      ;; check if tile is visible, else no need to render
      (if (and (> (+ xpos tile-width) 0) (< xpos (q/width))
             (> (+ ypos tile-height) 0) (< ypos (q/height)))
        (case tile
          nil (q/rect xpos ypos tile-width tile-height)
          (q/image (get tiles tile) xpos ypos))))))

(defn draw-sprite [{:keys [sprite-images x-offset y-offset]} sprite]
  (q/image-mode :center)
  (let [{:keys [id direction current-frame x y]} sprite
        xpos (- x x-offset)
        ypos (- y y-offset)]
    ;; check if sprite is visible, else no need to render
    (if (and (> xpos 0) (< xpos (q/width))
           (> ypos 0) (< ypos (q/height)))
      (q/image (get-in sprite-images [id direction current-frame]) xpos ypos))))

(defn draw-player [{:keys [player] :as state}]
  (draw-sprite state player))

(defn mid [x y z]
  (nth (sort [x y z]) 1))

(defn draw-game [{:keys [player tilemap] :as state}]
  (q/background 240)
  (let [x-offset (mid 0 (- (:x player) (/ (q/width) 2)) (- (* (count (first tilemap)) tile-width) (q/width)))
        y-offset (mid 0 (- (:y player) (/ (q/height) 2)) (- (* (count tilemap) tile-height) (q/height)))]
    (-> state
       (assoc :x-offset x-offset)
       (assoc :y-offset y-offset)
       (doto
           (draw-map)
           (draw-player)))))

(defn draw-loading-screen []
  (q/background 240)
  (q/fill 0)
  (q/text "Loading..." (/ (q/width) 10) (/ (q/height) 10)))

(defn draw-state [{:keys [tiles sprite-images] :as state}]
  (if (and (every? q/loaded? (vals tiles))
         (every? q/loaded? (reduce into [] (mapcat vals (vals sprite-images)))))
    (draw-game state)
    (draw-loading-screen))
  (q/fill 0)
  (q/text (str "fps:" (q/current-frame-rate)) 10 20))

(q/defsketch clojourney
  :title "Clojourney"
  :size [640 480]
  :setup setup
  :update update-state
  :draw draw-state
  :middleware [m/fun-mode])
